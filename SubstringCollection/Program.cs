﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubstringCollection
{
    class Program
    {
        static void Main()
        {
            string Word = "SoftConstruct";
            FindAllSubstrings(Word);
            Console.WriteLine("\nCount Of Substrings is: {0}\n", Word.Length * (((float)Word.Length + 1) / 2));
            string Substring = "Const";
            Console.WriteLine(IsSubstring(Word, Substring) ? "\n{0} is a Substring of {1}\n" : "\n{0} is not a substring of {1}\n", Substring, Word);
        } 
        public static void FindAllSubstrings(string word)
        {   
            for(int i = 0; i < word.Length; i++)
                Console.WriteLine(word.Substring(i, word.Length - i));
            if(word.Length > 0)
                FindAllSubstrings(word.Substring(0, word.Length - 1));
        }

        public static bool IsSubstring(string word, string subString)
        {
            bool isSubstring = false;
            int subStartIndex = 0;
            if (subString == "") return true;
            for(int i = 0; i < word.Length; i++)
            {
                if(word[i] == subString[0])
                {
                    subStartIndex = i;
                    for(int k = 1; k < subString.Length; k++)
                    { 
                        if(i + 1 < word.Length) i++;
                        if (word[i] == subString[k] && k == subString.Length - 1)
                        {
                            isSubstring = true;
                            Console.WriteLine("Substring start index -> {0}", subStartIndex);
                        }
                    }
                }
            }
            if (isSubstring) return true;
            return false;
        }
    }
}